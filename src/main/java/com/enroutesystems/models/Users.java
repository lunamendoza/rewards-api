package com.enroutesystems.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author lunaj
 *
 */
@Entity
public class Users {
    @Id
    private int emp_no;
    private Date birth_date;
    private String first_name;
    private String last_name;
    private char gender;
    private Date hire_date;


}
