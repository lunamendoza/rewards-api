package com.enroutesystems.service;

import com.enroutesystems.models.Users;
import com.enroutesystems.persistence.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lunaj
 */
@Service
public class RewardsService {

    private static Logger logger = LoggerFactory.getLogger(RewardsService.class);

    @Autowired
    private UsersRepository repository;

    public List<Users> getAllEmpleados() {

        return null;
    }

    public Users getEmpleado(int id) {
        return null;
    }

    public void addEmpleado(Users emp) {
    }

    public void updateEmpleado(Users emp) {
    }

    public void deleteEmpleado(int id) {
    }
}
