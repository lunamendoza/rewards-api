package com.enroutesystems.controller;

import com.enroutesystems.models.Users;
import com.enroutesystems.service.RewardsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @author lunaj
 */

@Component
@RestController
public class RewardsApiController {

    private static Logger logger = LoggerFactory.getLogger(RewardsApiController.class);

    @Autowired
    private RewardsService service;


    @RequestMapping(method = RequestMethod.GET, value = "/empleados/{id}")
    public Users getEmpleado(@PathVariable int id) {
        logger.info("Consultando empleado...");
        return service.getEmpleado(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "empleados")
    public List<Users> getAllEmpleados() {
        logger.info("Consultando todos los empleados...");
        return service.getAllEmpleados();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/empleados/add")
    public String addEmpleados(@RequestBody Users emp) {
        logger.info("Agregando empleado...");
        service.addEmpleado(emp);
        return "0";
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/empleados/update")
    public String updateEmpleado(@RequestBody Users emp) {
        logger.info("Actualizando empleado...");
        service.updateEmpleado(emp);
        return "0";
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/empleados/{id}/delete")
    public String updateEmpleado(@PathVariable int id) {
        logger.info("Eliminando empleado...");
        service.deleteEmpleado(id);
        return "0";
    }

}
