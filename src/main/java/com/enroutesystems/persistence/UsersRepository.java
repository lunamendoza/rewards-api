package com.enroutesystems.persistence;

import com.enroutesystems.models.Users;
import org.springframework.data.repository.CrudRepository;



public interface UsersRepository extends CrudRepository<Users, Integer> {

}